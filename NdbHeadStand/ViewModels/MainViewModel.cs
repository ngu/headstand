﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NdbHeadStand.ViewModels
{
    /// <summary>
    /// Главное представление данных для MainWindow
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel()
        {
            MenuItems = new ObservableCollection<MenuItemViewModel>();
        }

        public ObservableCollection<MenuItemViewModel> MenuItems
        {
            get;
            set;
        }

    }
}
