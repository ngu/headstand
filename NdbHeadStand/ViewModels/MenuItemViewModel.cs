﻿using NdbHeadStand.Commands;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Shapes;

namespace NdbHeadStand.ViewModels
{

    public abstract class MenuItemViewModel :ViewModelBase
    {
        public MenuItemViewModel()
        {
            NextItemCommand = new ParameterizedCommand(NextItemCommandExecution);
            PrevItemCommand = new ParameterizedCommand(PrevItemCommandExecution);
        }

        public string Name
        {
            get;
            set;
        }

        public object Icon
        {
            get;
            set;
        }

        public ObservableCollection<ItemViewModel> Items
        {
            get;
            set;
        }

        public ICommand NextItemCommand
        {
            get;
            private set;
        }

        public ICommand PrevItemCommand
        {
            get;
            private set;
        }

        protected abstract void NextItemCommandExecution(object parameter);

        protected abstract void PrevItemCommandExecution(object parameter);
    }
}